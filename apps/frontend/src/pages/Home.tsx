import { useState } from 'react';
import { Dashboard } from '@monorepo/shell';

import { useAppSelector, useAppDispatch } from '../redux/hooks';
import { User, addUser, deleteUser } from '../redux/users/usersSlice';

export default function Home() {
  const users = useAppSelector(state => state.users);
  const dispatch = useAppDispatch();

  const [name, setName] = useState<string>('');

  return (
    <Dashboard>
      <form
        onSubmit={e => {
          e.preventDefault();
          dispatch(addUser({ name } as User));
          setName('');
        }}
      >
        <fieldset>
          <legend>Add new user</legend>
          <p>
            <input
              placeholder="Name"
              value={name}
              onChange={({ target: { value } }) => setName(value)}
            />
          </p>
          <button type="submit">Add user</button>
        </fieldset>
      </form>

      {users.entities.map(user => (
        <div key={`user-${user.id}`}>
          <pre>{JSON.stringify(user, null, 2)}</pre>
          <button onClick={() => dispatch(deleteUser(user.id))}>Delete</button>
        </div>
      ))}
    </Dashboard>
  );
}
