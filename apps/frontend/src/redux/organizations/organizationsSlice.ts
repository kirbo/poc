import { PayloadAction, createSlice } from '@reduxjs/toolkit';

export type Organization = {
  id: number;
  name: string;
  createdAt: string;
};

interface OrganizationsState {
  entities: Organization[];
  loading: 'idle' | 'pending' | 'succeeded' | 'failed';
}

const initialState = {
  entities: [],
  loading: 'idle'
} as OrganizationsState;

const organizationsSlice = createSlice({
  name: 'organizations',
  initialState,
  reducers: {
    organizationAdded(
      state: OrganizationsState,
      action: PayloadAction<Organization>
    ) {
      state.entities.push({
        id: action.payload.id,
        name: action.payload.name,
        createdAt: new Date().toISOString()
      });
    },
    organizationDeleted(
      state: OrganizationsState,
      action: PayloadAction<number>
    ) {
      state.entities.filter(organization => organization.id !== action.payload);
    }
  }
});

export const { organizationAdded, organizationDeleted } =
  organizationsSlice.actions;
export default organizationsSlice.reducer;
