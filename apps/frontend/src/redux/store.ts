import { configureStore } from '@reduxjs/toolkit';
import organizationsReducer from './organizations/organizationsSlice';
import usersReducer from './users/usersSlice';

export const store = configureStore({
  reducer: {
    organizations: organizationsReducer,
    users: usersReducer
  }
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
