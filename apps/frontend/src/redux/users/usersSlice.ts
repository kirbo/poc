import { PayloadAction, createSlice } from '@reduxjs/toolkit';

export type User = {
  id: number;
  name: string;
  createdAt: string;
};

interface UsersState {
  entities: User[];
  loading: 'idle' | 'pending' | 'succeeded' | 'failed';
}

const initialState = {
  entities: [],
  loading: 'idle'
} as UsersState;

const usersSlice = createSlice({
  name: 'users',
  initialState,
  reducers: {
    addUser(state: UsersState, action: PayloadAction<User>) {
      state.entities.push({
        id:
          state.entities.length === 0
            ? 1
            : Math.max(...state.entities.map(({ id }) => id)) + 1,
        name: action.payload.name,
        createdAt: new Date().toISOString()
      });
    },
    deleteUser(state: UsersState, action: PayloadAction<number>) {
      state.entities = state.entities.filter(
        user => user.id !== action.payload
      );
    }
  }
});

export const { addUser, deleteUser } = usersSlice.actions;
export default usersSlice.reducer;
